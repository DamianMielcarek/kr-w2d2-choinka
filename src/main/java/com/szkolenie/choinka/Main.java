package com.szkolenie.choinka;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj datę [yyyy-MM-dd]: ");
        String userDate = sc.nextLine();

        LocalDate now = LocalDate.now();
        LocalDate christmasEve = LocalDate.of(2016, Month.DECEMBER, 24);

        System.out.println(now.until(christmasEve, ChronoUnit.DAYS));

        LocalDate parse = LocalDate.from(
                DateTimeFormatter.ofPattern("yyyy-MM-dd")
                        .parse(userDate));
        System.out.println(parse.getDayOfWeek());
    }
}
